import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trackeroo/app_scaffold.dart';
import 'package:trackeroo/frontend/utils/onboarding/ob_categories.dart';
import 'package:trackeroo/frontend/utils/onboarding/ob_details.dart';
import 'package:trackeroo/frontend/utils/onboarding/ob_home.dart';
import 'package:trackeroo/frontend/utils/onboarding/ob_welcome.dart';
import 'package:trackeroo/frontend/utils/onboarding/ob_trackeroo.dart';
import 'package:trackeroo/logic/services/app_state_controller.dart';
import 'package:trackeroo/logic/services/locator.dart';

class OnboardingView extends StatefulWidget {
  const OnboardingView({super.key});

  @override
  State<OnboardingView> createState() => _OnboardingViewState();
}

class _OnboardingViewState extends State<OnboardingView> {
  final controller = PageController();
  final AppStateController appStateController =
      locator.get<AppStateController>();

  int pageIndex = 0;

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        onPageChanged: (value) => setState(() {
          pageIndex = value;
        }),
        controller: controller,
        children: const [
          OnboardingWelcome(),
          OnboardingTrackeroo(),
          OnboardingHome(),
          OnboardingDetails(),
          OnboardingCategories()
        ],
      ),
      bottomSheet: Container(
        height: 120.0,
        padding: const EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
                onPressed: () {
                  appStateController.appState.isFirstOpening = false;
                  appStateController.safeAppState();
                  Navigator.of(context).popUntil((route) => route.isFirst);
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const AppScaffold()));
                },
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                icon: Row(
                  children: [
                    const Icon(Icons.close_rounded),
                    const SizedBox(width: 10.0),
                    Text(
                      'skip'.tr,
                      style: const TextStyle(fontSize: 16.0),
                    ),
                  ],
                )),
            Row(
              children: [
                buildPageIndicator(0),
                buildPageIndicator(1),
                buildPageIndicator(2),
                buildPageIndicator(3),
                buildPageIndicator(4)
              ],
            ),
            IconButton(
                onPressed: () {
                  if (pageIndex == 4) {
                    appStateController.appState.isFirstOpening = false;
                    appStateController.safeAppState();
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const AppScaffold()));
                  } else {
                    controller.nextPage(
                        duration: const Duration(milliseconds: 300),
                        curve: Curves.easeInOut);
                  }
                },
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                icon: Row(
                  children: [
                    Text(
                      pageIndex != 4 ? 'next'.tr : 'done'.tr,
                      style: const TextStyle(fontSize: 16.0),
                    ),
                    const SizedBox(width: 10.0),
                    Icon(pageIndex != 4
                        ? Icons.arrow_forward_rounded
                        : Icons.done_rounded),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Widget buildPageIndicator(int index) => GestureDetector(
        onTap: () => controller.jumpToPage(index),
        child: Container(
          height: 10.0,
          width: 10.0,
          margin: const EdgeInsets.all(4.0),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: pageIndex == index
                  ? Theme.of(context).colorScheme.primary
                  : Theme.of(context).colorScheme.primaryContainer),
        ),
      );
}
