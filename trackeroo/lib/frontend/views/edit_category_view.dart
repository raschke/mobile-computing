import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trackeroo/logic/constants/selecable_colors.dart';
import 'package:trackeroo/logic/constants/selectable_icons.dart';
import 'package:trackeroo/logic/models/category.dart';
import 'package:trackeroo/logic/models/selecable_color.dart';
import 'package:trackeroo/logic/services/categories_controller.dart';
import 'package:trackeroo/logic/services/locator.dart';

class EditCategoryView extends StatefulWidget {
  const EditCategoryView({Key? key, this.category}) : super(key: key);

  final Category? category;

  @override
  State<EditCategoryView> createState() => _EditCategoryViewState();
}

class _EditCategoryViewState extends State<EditCategoryView> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _budgetController = TextEditingController();
  Color _selectedColor = selectableColors['Ocean']!.color;
  IconData _selectedIcon = selectableIcons.first;
  double _budget = -1;
  bool _hasBudget = false;

  @override
  void initState() {
    super.initState();
    if (widget.category != null) {
      _nameController.text = widget.category!.title;
      _selectedColor = Color(widget.category!.colorValue);
      _selectedIcon = IconData(widget.category!.iconCodePoint,
          fontFamily: widget.category!.iconFontFamily);
      _hasBudget = !widget.category!.budget.isNegative;
      if (_hasBudget) {
        _budget = widget.category!.budget;
        _budgetController.text = widget.category!.budget.toString();
      }
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    _budgetController.dispose();
    super.dispose();
  }

  Widget _buildIconContainer(IconData iconData, double size) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        color: _selectedColor,
        shape: BoxShape.circle,
      ),
      child: Icon(
        iconData,
        color: Theme.of(context).colorScheme.background,
        size: size * 0.6,
      ),
    );
  }

  void _showColorPicker() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          insetPadding:
              const EdgeInsets.symmetric(vertical: 100.0, horizontal: 20.0),
          scrollable: true,
          title: Text('CC'.tr),
          content: Column(
            children: [
              for (SelectableColor selecableColor in selectableColors.values)
                SizedBox(
                  width: 500.0,
                  child: GestureDetector(
                    onTap: () => setState(() {
                      _selectedColor =
                          selectableColors[selecableColor.title]!.color;
                      Navigator.of(context).pop();
                    }),
                    child: ListTile(
                      dense: true,
                      leading: Container(
                        width: 20.0,
                        height: 20.0,
                        decoration: BoxDecoration(
                            color: selecableColor.color,
                            shape: BoxShape.circle),
                      ),
                      title: Text(selecableColor.title),
                      trailing: _selectedColor ==
                              selectableColors[selecableColor.title]?.color
                          ? const Icon(Icons.check_rounded)
                          : null,
                    ),
                  ),
                )
            ],
          ),
        );
      },
    );
  }

  void _createCategory() {
    if (_nameController.text.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('TCBE'.tr)));
      return;
    }
    Category newCategory = Category(
        id: _nameController.text.replaceAll(' ', '_').toLowerCase(),
        title: _nameController.text,
        iconCodePoint: _selectedIcon.codePoint,
        colorValue: _selectedColor.value,
        budget: _hasBudget ? double.parse(_budgetController.text) : -1);
    locator.get<CategoriesController>().saveCategory(newCategory);
    Navigator.of(context).pop();
  }

  void _updateCategory() {
    Category updatedCategory = Category(
      id: widget.category!.id,
      title: _nameController.text,
      iconCodePoint: _selectedIcon.codePoint,
      iconFontFamily: 'MaterialIcons',
      colorValue: _selectedColor.value,
      spendings: widget.category!.spendings,
      budget: _hasBudget ? double.parse(_budgetController.text) : _budget,
    );

    locator.get<CategoriesController>().updateCategory(updatedCategory);

    Navigator.pop(context);
  }

  void _deleteCategory() {
    locator.get<CategoriesController>().deleteCategory(widget.category!.id);
    Navigator.of(context).pop();
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('EC'.tr),
        actions: [
          IconButton(
              onPressed: () => widget.category != null
                  ? showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text('DC'.tr),
                          content: Text('mes'.tr),
                          actions: [
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('Cancel'.tr),
                            ),
                            TextButton(
                              onPressed: () => _deleteCategory(),
                              child: Text(
                                'Delete'.tr,
                                style: TextStyle(
                                    color: Theme.of(context).colorScheme.error),
                              ),
                            ),
                          ],
                        );
                      },
                    )
                  : Navigator.of(context).pop(),
              padding: const EdgeInsets.all(16.0),
              icon: Icon(widget.category != null
                  ? Icons.delete_rounded
                  : Icons.close_rounded))
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Icon',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 70.0),
              Center(
                child: GestureDetector(
                  onTap: () => showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                        insetPadding: const EdgeInsets.symmetric(
                            vertical: 100.0, horizontal: 20.0),
                        scrollable: true,
                        title: Text('Choose icon'.tr),
                        content: SizedBox(
                          height: 450.0,
                          width: 500.0,
                          child: GridView.count(
                            crossAxisCount: 5,
                            children: [
                              for (IconData selectableIconData
                                  in selectableIcons)
                                GestureDetector(
                                  onTap: () => setState(() {
                                    _selectedIcon = selectableIconData;
                                    Navigator.of(context).pop();
                                  }),
                                  child: Container(
                                    height: 20.0,
                                    width: 20.0,
                                    padding: const EdgeInsets.all(5.0),
                                    margin: const EdgeInsets.all(5.0),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color:
                                            selectableIconData == _selectedIcon
                                                ? Theme.of(context)
                                                    .colorScheme
                                                    .background
                                                : null),
                                    child: Icon(selectableIconData),
                                  ),
                                )
                            ],
                          ),
                        )),
                  ),
                  child: _buildIconContainer(_selectedIcon, 100.0),
                ),
              ),
              const SizedBox(height: 70.0),
              const Text(
                'Name',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              TextField(
                controller: _nameController,
                decoration: InputDecoration(
                    isDense: true,
                    hintText: 'Category Name'.tr,
                    hintStyle: const TextStyle(fontWeight: FontWeight.normal)),
              ),
              const SizedBox(height: 30.0),
              Text(
                'Color'.tr,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              GestureDetector(
                onTap: _showColorPicker,
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  // color is needed because of the container NOT beeing width double.infinity
                  // when no color is specified
                  color: Theme.of(context).colorScheme.background,
                  child: Row(
                    children: [
                      Container(
                        height: 20.0,
                        width: 20.0,
                        decoration: BoxDecoration(
                            color: _selectedColor, shape: BoxShape.circle),
                      ),
                      const SizedBox(width: 10.0),
                      Text(selectableColors.values
                              .where(
                                  (element) => element.color == _selectedColor)
                              .isNotEmpty
                          ? selectableColors.values
                              .where(
                                  (element) => element.color == _selectedColor)
                              .first
                              .title
                          : 'CC'.tr),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 20.0),
              Text(
                'Budget'.tr,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              CheckboxListTile(
                  title: Text(
                    'set Budget'.tr,
                    style: const TextStyle(fontSize: 16.0),
                  ),
                  contentPadding: const EdgeInsets.all(0.0),
                  dense: true,
                  value: _hasBudget,
                  onChanged: (value) => setState(() {
                        _hasBudget = !_hasBudget;
                      })),
              AbsorbPointer(
                absorbing: !_hasBudget,
                child: Opacity(
                  opacity: !_hasBudget ? 0.5 : 1.0,
                  child: Row(
                    children: [
                      const Icon(Icons.savings_outlined, size: 18.0),
                      const SizedBox(width: 10.0),
                      Expanded(
                        child: TextField(
                          controller: _budgetController,
                          decoration: InputDecoration(
                              hintText: 'Budget'.tr,
                              hintStyle: const TextStyle(
                                  fontWeight: FontWeight.normal),
                              isDense: true,
                              border: InputBorder.none),
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      const Text('€')
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () =>
            widget.category != null ? _updateCategory() : _createCategory(),
        child: const Icon(Icons.save_rounded),
      ),
    );
  }
}
