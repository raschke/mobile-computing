import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:trackeroo/frontend/utils/category_chip.dart';
import 'package:trackeroo/frontend/utils/transaction_listtile.dart';
// import 'package:trackeroo/frontend/views/onboarding_view.dart';
import 'package:trackeroo/logic/models/category.dart';
import 'package:trackeroo/logic/models/transaction.dart';
import 'package:trackeroo/logic/services/categories_controller.dart';
import 'package:trackeroo/logic/services/locator.dart';
import 'package:trackeroo/logic/services/transactions_controller.dart';

enum Timespan { daily, weekly, monthly, yearly, all }

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  Timespan timespanView = Timespan.monthly;
  TransactionsController transContr = locator.get<TransactionsController>();
  List<Category> inChartShownCats = [];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ValueListenableBuilder(
            valueListenable: transContr.transactionsBox.listenable(),
            builder: (context, Box<Transaction> box, widget) {
              transContr.transactionsList = box.values.toList();
              transContr.transactionsList
                  .sort((b, a) => a.createdAt.compareTo(b.createdAt));
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'balance'.tr,
                    style: const TextStyle(
                        fontSize: 14.0, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 5.0),
                  Text(
                    transContr.balance.toStringAsFixed(2),
                    style: const TextStyle(
                        fontSize: 40.0, fontWeight: FontWeight.w300),
                  ),
                  const SizedBox(height: 10.0),
                  Text(
                    'overview'.tr,
                    style: const TextStyle(
                        fontSize: 14.0, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 5.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              const Icon(Icons.arrow_downward,
                                  color: Colors.green, size: 16.0),
                              Text('income'.tr),
                            ],
                          ),
                          Text('${transContr.income.toStringAsFixed(2)} €')
                        ],
                      ),
                      Center(
                        child: SizedBox(
                          height: 200.0,
                          width: 50.0,
                          child: PieChart(
                            PieChartData(
                                centerSpaceRadius: 30.0,
                                sections: transContr.transactionsList.isNotEmpty
                                    ? buildPieChartSectionList(context)
                                    : null,
                                pieTouchData: PieTouchData(enabled: true)),
                            swapAnimationDuration:
                                const Duration(milliseconds: 500),
                            swapAnimationCurve: Curves.easeInOut,
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              const Icon(Icons.arrow_upward,
                                  color: Colors.red, size: 16.0),
                              Text('expenses'.tr),
                            ],
                          ),
                          Text('${transContr.expenses.toStringAsFixed(2)} €')
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(height: 20.0),
                  Wrap(
                    children: [
                      for (Category cat in inChartShownCats)
                        CategoryChip(category: cat)
                    ],
                  ),
                  const SizedBox(height: 10.0),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     FilterChip(
                  //       onSelected: (value) => setState(() {
                  //         timespanView = Timespan.daily;
                  //       }),
                  //       label: const Text('Daily'),
                  //       selected: timespanView == Timespan.daily,
                  //       showCheckmark: false
                  //     ),
                  //     FilterChip(
                  //       onSelected: (value) => setState(() {
                  //         timespanView = Timespan.weekly;
                  //       }),
                  //       label: const Text('Weekly'),
                  //       selected: timespanView == Timespan.weekly,
                  //       showCheckmark: false
                  //     ),
                  //     FilterChip(
                  //       onSelected: (value) => setState(() {
                  //         timespanView = Timespan.monthly;
                  //       }),
                  //       label: const Text('Monthly'),
                  //       selected: timespanView == Timespan.monthly,
                  //       showCheckmark: false
                  //     ),
                  //     FilterChip(
                  //       onSelected: (value) => setState(() {
                  //         timespanView = Timespan.yearly;
                  //       }),
                  //       label: const Text('Yearly'),
                  //       selected: timespanView == Timespan.yearly,
                  //       showCheckmark: false
                  //     ),
                  //     FilterChip(
                  //       onSelected: (value) => setState(() {
                  //         timespanView = Timespan.all;
                  //       }),
                  //       label: const Text('All'),
                  //       selected: timespanView == Timespan.all,
                  //       showCheckmark: false
                  //     )
                  //   ],
                  // ),
                  // const SizedBox(height: 20.0),
                  Text(
                    'transactions'.tr,
                    style: const TextStyle(
                        fontSize: 14.0, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 5.0),
                  box.isEmpty
                      ? SizedBox(
                          height: 200.0,
                          child: Center(
                            child: Text('NET'.tr),
                          ),
                        )
                      : ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: transContr.transactionsList.length <= 25
                              ? transContr.transactionsList.length
                              : 25,
                          itemBuilder: (context, index) => TransactionListtile(
                              transaction: transContr.transactionsList[index])),
                  const SizedBox(height: 65.0)
                ],
              );
            }),
      ),
    );
  }

  List<PieChartSectionData> buildPieChartSectionList(BuildContext context) {
    List<PieChartSectionData> sectionList = [];
    inChartShownCats.clear();

    int elapsedDays = transContr.transactionsList.last.createdAt
        .difference(DateTime.now())
        .inDays;
    if (elapsedDays <= 0) elapsedDays = 1;

    double other = 0;

    for (Category cat
        in locator.get<CategoriesController>().categories.values) {
      double categorySum = 0;
      double avg = 0;
      for (Transaction tr in transContr.transactionsList) {
        if (tr.categoryId == cat.id && tr.amount.isNegative) {
          categorySum += tr.amount;
        }
      }

      if (timespanView == Timespan.daily) {
        avg = categorySum / elapsedDays;
      } else if (timespanView == Timespan.weekly) {
        avg = categorySum / (elapsedDays / 7);
      } else if (timespanView == Timespan.monthly) {
        avg = categorySum / (elapsedDays / 30);
      } else if (timespanView == Timespan.yearly) {
        avg = categorySum / (elapsedDays / 365);
      } else {
        avg = categorySum;
      }

      bool meetsThreshold =
          (categorySum.abs() / transContr.balance.abs()) > 0.06;

      if (!meetsThreshold) {
        other += avg;
      }

      if (!avg.isNaN && meetsThreshold) {
        inChartShownCats.add(cat);
        sectionList.add(PieChartSectionData(
            value: double.parse(avg.toStringAsFixed(2)).abs(),
            showTitle: false,
            title: avg.toStringAsFixed(2),
            color: Color(cat.colorValue),
            radius: 70.0,
            badgeWidget: Container(
                height: 30.0,
                width: 30.0,
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.surface,
                  shape: BoxShape.circle,
                ),
                child: Center(
                    child: Icon(
                  IconData(
                    cat.iconCodePoint,
                    fontFamily: cat.iconFontFamily,
                  ),
                  color: Color(cat.colorValue),
                  size: 18.0,
                ))),
            titlePositionPercentageOffset: .4,
            titleStyle:
                TextStyle(color: Theme.of(context).colorScheme.onSurface),
            badgePositionPercentageOffset: 1));
      }
    }
    if (other.abs() > 0) {
      inChartShownCats.add(Category(
          id: 'other',
          title: 'Other',
          iconCodePoint: Icons.category_rounded.codePoint,
          colorValue: Colors.grey.value));
      sectionList.add(PieChartSectionData(
          value: double.parse(other.toStringAsFixed(2)).abs(),
          showTitle: false,
          title: other.toStringAsFixed(2),
          color: Colors.grey,
          radius: 70.0,
          badgeWidget: Container(
              height: 30.0,
              width: 30.0,
              decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.surface,
                  shape: BoxShape.circle),
              child: const Center(
                  child: Icon(
                Icons.category_rounded,
                color: Colors.grey,
                size: 18.0,
              ))),
          titlePositionPercentageOffset: .4,
          titleStyle: TextStyle(color: Theme.of(context).colorScheme.onSurface),
          badgePositionPercentageOffset: 1));
    }
    return sectionList;
  }
}
