import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trackeroo/logic/services/locator.dart';
import '../../logic/models/profile.dart';
import 'package:trackeroo/logic/services/profile_controller.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:flutter/services.dart';

class EditSettingsView extends StatefulWidget {
  const EditSettingsView({super.key});

  @override
  State<EditSettingsView> createState() => _EditSettingsView();
}

class _EditSettingsView extends State<EditSettingsView> {
  TextEditingController nameController = TextEditingController();
  TextEditingController numberController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  String imageController = '';

  ProfileController profileController = locator.get<ProfileController>();

  Widget fillNameTextField(String text) {
    if (nameController.text.isEmpty) {
      nameController.text = text;
    }
    return TextField(
      controller: nameController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.only(left: 10.0, bottom: 7.0),
        labelText: 'enterNa'.tr,
        prefixIcon: const Icon(Icons.person_2),
        border: const OutlineInputBorder(),
      ),
      style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.w300),
    );
  }

  Widget fillNumberTextField(String text) {
    if (numberController.text.isEmpty) {
      numberController.text = text;
    }
    return TextField(
      controller: numberController,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.only(left: 10.0, bottom: 7.0),
        labelText: 'enterNu'.tr,
        prefixIcon: const Icon(Icons.phone_android),
        border: const OutlineInputBorder(),
      ),
      style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.w300),
    );
  }

  Widget fillEmailTextField(String text) {
    if (emailController.text.isEmpty) {
      emailController.text = text;
    }
    return TextField(
      controller: emailController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.only(left: 10.0, bottom: 7.0),
        labelText: 'enterE'.tr,
        prefixIcon: const Icon(Icons.email),
        border: const OutlineInputBorder(),
      ),
      style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.w300),
    );
  }

  getImage(String path) {
    String filePath = path.replaceFirst('file://', '');
    return filePath;
  }

  Future<bool> _updateProfile() async {
    if (nameController.text.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('name cant be empty')),
      );
      return false;
    }

    if (numberController.text.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Mobile Number cant be empty ')),
      );
      return false;
    }

    if (emailController.text.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('E-Mail cant be empty ')),
      );
      return false;
    }

    if (imageController.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Choose a Picture')),
      );
      return false;
    }

    Profile profile = Profile(
      name: nameController.text,
      number: int.parse(numberController.text),
      email: emailController.text,
      imagePath: imageController,
    );
    locator.get<ProfileController>().saveProfile(profile);
    return true;
  }

  File? _imageFile;
  final ImagePicker _picker = ImagePicker();

  Widget imageProfile() {
    return Center(
      child: Stack(
        children: <Widget>[
          CircleAvatar(
            radius: 80.0,
            backgroundImage: _imageFile == null
                ? FileImage(File(getImage(profileController.profile.imagePath)))
                : FileImage(File(_imageFile!.path)) as ImageProvider<Object>?,
          ),
          Positioned(
            bottom: 20.0,
            right: 20.0,
            child: InkWell(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    builder: ((builder) => bottomSheet()),
                  );
                },
                child: const Icon(
                  Icons.camera_alt,
                  color: Colors.teal,
                  size: 28,
                )),
          )
        ],
      ),
    );
  }

  void takePhoto(ImageSource source) async {
    try {
      final pickedFile = await _picker.pickImage(
        source: source,
      );
      if (pickedFile == null) return;
      final pickedTemp = File(pickedFile.path);
      setState(() {
        _imageFile = pickedTemp;
        imageController = pickedTemp.path;
      });
    } on PlatformException catch (e) {
      print('Error$e'.tr);
    }
  }

  Widget bottomSheet() {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(children: <Widget>[
        Text(
          'ChoosePB'.tr,
          style: const TextStyle(
            fontSize: 20.0,
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton.icon(
              icon: const Icon(Icons.camera),
              onPressed: () {
                takePhoto(ImageSource.camera);
              },
              label: Text('camera'.tr),
            ),
            TextButton.icon(
              icon: const Icon(Icons.image),
              onPressed: () {
                takePhoto(ImageSource.gallery);
              },
              label: Text('Gallery'.tr),
            )
          ],
        )
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('EP'.tr),
        elevation: 0.0,
        scrolledUnderElevation: 0.0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            imageProfile(),
            const SizedBox(
              height: 20,
            ),
            fillNameTextField(locator.get<ProfileController>().profile.name),
            const SizedBox(
              height: 20,
            ),
            fillNumberTextField(
                locator.get<ProfileController>().profile.number.toString()),
            const SizedBox(
              height: 20,
            ),
            fillEmailTextField(locator.get<ProfileController>().profile.email),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () async =>
                  {if (await _updateProfile()) Navigator.pop(context)},
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.grey.shade300),
              ),
              child: Text('Save'.tr),
            ),
          ],
        ),
      ),
    );
  }
}
