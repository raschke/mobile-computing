import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:trackeroo/frontend/views/edit_settings_view.dart';
import 'package:trackeroo/logic/services/locator.dart';
import 'package:trackeroo/logic/services/profile_controller.dart';

class SettingsView extends StatefulWidget {
  const SettingsView({super.key});

  @override
  State<SettingsView> createState() => _SettingsView();
}

class _SettingsView extends State<SettingsView> {
  final List locale = [
    {'name': 'Englisch', 'locale': const Locale('en', 'US')},
    {'name': 'Deutsch', 'locale': const Locale('de', 'DE')}
  ];
  ProfileController profileController = locator.get<ProfileController>();

  updatelanguage(Locale locale) {
    Get.back();
    Get.updateLocale(locale);
  }

  getImage(String path) {
    String filePath = path.replaceFirst('file://', '');
    return filePath;
  }

  biulddialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (builder) {
          return AlertDialog(
            title: Text('Lang'.tr),
            content: SizedBox(
              width: double.maxFinite,
              child: ListView.separated(
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                          onTap: () {
                            print(locale[index]['name']);
                            updatelanguage(locale[index]['locale']);
                          },
                          child: Text(locale[index]['name'])),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return const Divider(
                      color: Colors.blue,
                    );
                  },
                  itemCount: locale.length),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'.tr),
        elevation: 0.0,
        scrolledUnderElevation: 0.0,
      ),
      body: ValueListenableBuilder(
          valueListenable: profileController.profilBox.listenable(),
          builder: (context, box, child) {
            ProfileController profileController =
                locator.get<ProfileController>();
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 48),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      radius: 79,
                      backgroundColor: Colors.blue.shade100,
                      child: profileController.profile.imagePath.isNotEmpty
                          ? CircleAvatar(
                              radius: 76,
                              backgroundImage: FileImage(File(getImage(
                                  profileController.profile.imagePath))),
                            )
                          : const Icon(Icons.person),
                    ),
                    const SizedBox(height: 18),
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: InputDecorator(
                        decoration: InputDecoration(
                          contentPadding:
                              const EdgeInsets.only(left: 10.0, bottom: 7.0),
                          labelText: 'name'.tr,
                          prefixIcon: const Icon(Icons.phone_android),
                          border: InputBorder.none,
                        ),
                        child: Text(
                          locator.get<ProfileController>().profile.name,
                          style: const TextStyle(fontSize: 18),
                        ),
                      ),
                    ),
                    const SizedBox(height: 18),
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: InputDecorator(
                        decoration: InputDecoration(
                          contentPadding:
                              const EdgeInsets.only(left: 10.0, bottom: 7.0),
                          labelText: 'Number'.tr,
                          prefixIcon: const Icon(Icons.phone_android),
                          border: InputBorder.none,
                        ),
                        child: Text(
                          locator
                              .get<ProfileController>()
                              .profile
                              .number
                              .toString(),
                          style: const TextStyle(fontSize: 18),
                        ),
                      ),
                    ),
                    const SizedBox(height: 18),
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: InputDecorator(
                        decoration: InputDecoration(
                          contentPadding:
                              const EdgeInsets.only(left: 10.0, bottom: 7.0),
                          labelText: 'email'.tr,
                          prefixIcon: const Icon(Icons.phone_android),
                          border: InputBorder.none,
                        ),
                        child: Text(
                          locator.get<ProfileController>().profile.email,
                          style: const TextStyle(fontSize: 18),
                        ),
                      ),
                    ),
                    const SizedBox(height: 18),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const EditSettingsView()),
                        );
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.grey.shade300),
                      ),
                      child: Text('EP'.tr),
                    ),
                    const SizedBox(
                      height: 18,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        biulddialog(context);
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.grey.shade300),
                      ),
                      child: Text('Lang'.tr),
                    )
                  ],
                ),
              ),
            );
          }),
    );
  }
}
