import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:trackeroo/frontend/utils/category_listtile.dart';
import 'package:trackeroo/logic/models/category.dart';
import 'package:trackeroo/logic/services/categories_controller.dart';
import 'package:trackeroo/logic/services/locator.dart';

class CategoriesView extends StatelessWidget {
  const CategoriesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Categories'.tr,
              style:
                  const TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 10.0),
            ValueListenableBuilder(
                valueListenable:
                    locator.get<CategoriesController>().catBox.listenable(),
                builder: (context, Box<Category> box, child) {
                  locator.get<CategoriesController>().categories = box.toMap();
                  return ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: box.length,
                    itemBuilder: (context, index) => CategoryListtile(
                        category: locator
                            .get<CategoriesController>()
                            .categories
                            .values
                            .elementAt(index)),
                  );
                }),
            const SizedBox(height: 65.0)
          ],
        ),
      ),
    );
  }
}
