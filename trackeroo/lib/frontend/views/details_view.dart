import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:trackeroo/frontend/utils/transaction_listtile.dart';
import 'package:trackeroo/logic/models/app_state.dart';
import 'package:trackeroo/logic/models/category.dart';
import 'package:trackeroo/logic/models/transaction.dart';
import 'package:trackeroo/logic/services/app_state_controller.dart';
import 'package:trackeroo/logic/services/categories_controller.dart';
import 'package:trackeroo/logic/services/locator.dart';
import 'package:trackeroo/logic/services/transactions_controller.dart';

class DetailsView extends StatefulWidget {
  const DetailsView({super.key});

  @override
  State<DetailsView> createState() => _DetailsViewState();
}

class _DetailsViewState extends State<DetailsView> {
  TransactionsController transactionsController =
      locator.get<TransactionsController>();
  DateTimeRange timespan =
      DateTimeRange(start: DateTime.now(), end: DateTime.now());

  // List<String> selectedCategoryIds = ;
  AppState appState = locator.get<AppStateController>().appState;

  @override
  void initState() {
    // TODO: commented out due to bug and none relevancy for presentation
    // if (transactionsController.transactionsList.isNotEmpty) {
    //   timespan = DateTimeRange(
    //       start: transactionsController.transactionsList.last.createdAt,
    //       end: transactionsController.transactionsList.first.createdAt
    //     );
    // }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(16.0),
      child: ValueListenableBuilder(
          valueListenable:
              locator.get<AppStateController>().appStateBox.listenable(),
          builder: (context, Box<dynamic> appStateBox, child) {
            return ValueListenableBuilder(
                valueListenable: locator
                    .get<TransactionsController>()
                    .transactionsBox
                    .listenable(),
                builder: (context, Box<Transaction> box, child) {
                  List<Transaction> filteredTransList = box.values
                      .toList()
                      .where((element) => appState.detailsTransactionsFilter
                          .contains(element.categoryId))
                      .toList();
                  filteredTransList
                      .sort((b, a) => a.createdAt.compareTo(b.createdAt));
                  return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Datails chart'.tr,
                          style: const TextStyle(
                              fontSize: 14.0, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 20.0),
                        Center(
                            child: Container(
                                width: 400,
                                height: 250,
                                padding: const EdgeInsets.only(right: 20.0),
                                child: LineChart(LineChartData(
                                    borderData: FlBorderData(
                                        show: true,
                                        border: Border.symmetric(
                                            horizontal: BorderSide(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .onSurface,
                                                width: 0.5))),
                                    gridData: FlGridData(
                                      show: true,
                                      getDrawingHorizontalLine: (value) {
                                        return FlLine(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .onSurface,
                                            strokeWidth: 0.5);
                                      },
                                      drawVerticalLine: false,
                                    ),
                                    titlesData: FlTitlesData(
                                      show: true,
                                      rightTitles: AxisTitles(
                                        sideTitles:
                                            SideTitles(showTitles: false),
                                      ),
                                      topTitles: AxisTitles(
                                          sideTitles:
                                              SideTitles(showTitles: false)),
                                      bottomTitles: AxisTitles(
                                        sideTitles: SideTitles(
                                          showTitles: true,
                                          // interval: 1,
                                          reservedSize: 30,
                                        ),
                                      ),
                                      leftTitles: AxisTitles(
                                        sideTitles: SideTitles(
                                          showTitles: true,
                                          // interval: 50,
                                          reservedSize: 42,
                                        ),
                                      ),
                                    ),
                                    // maxX: 12,
                                    // maxY: 200,
                                    minY: 0,
                                    minX: 0,
                                    lineBarsData:
                                        buildBarData(filteredTransList))))),
                        const SizedBox(height: 5.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            IconButton.filledTonal(
                                visualDensity: VisualDensity.compact,
                                onPressed: () => showModalBottomSheet(
                                    useSafeArea: true,
                                    showDragHandle: true,
                                    isScrollControlled: true,
                                    context: context,
                                    builder: (context) => StatefulBuilder(
                                          builder: (BuildContext context,
                                                  StateSetter setState) =>
                                              Container(
                                            // height: 650.0,
                                            width: double.infinity,
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 20.0),
                                            child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'Choose categories'.tr,
                                                    style: const TextStyle(
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  for (Category category in locator
                                                      .get<
                                                          CategoriesController>()
                                                      .categories
                                                      .values)
                                                    CheckboxListTile(
                                                        title: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Icon(
                                                              IconData(
                                                                  category
                                                                      .iconCodePoint,
                                                                  fontFamily:
                                                                      category
                                                                          .iconFontFamily),
                                                              color: Color(category
                                                                  .colorValue),
                                                              size: 18.0,
                                                            ),
                                                            const SizedBox(
                                                                width: 10.0),
                                                            Text(
                                                              category.title,
                                                              style:
                                                                  const TextStyle(
                                                                      fontSize:
                                                                          15.0),
                                                            ),
                                                          ],
                                                        ),
                                                        value: appState
                                                            .detailsTransactionsFilter
                                                            .contains(
                                                                category.id),
                                                        contentPadding:
                                                            const EdgeInsets
                                                                .all(0.0),
                                                        dense: true,
                                                        onChanged:
                                                            (value) =>
                                                                setState(() {
                                                                  if (appState
                                                                      .detailsTransactionsFilter
                                                                      .contains(
                                                                          category
                                                                              .id)) {
                                                                    appState
                                                                        .detailsTransactionsFilter
                                                                        .remove(
                                                                            category.id);
                                                                  } else {
                                                                    appState
                                                                        .detailsTransactionsFilter
                                                                        .add(category
                                                                            .id);
                                                                  }
                                                                  locator
                                                                      .get<
                                                                          AppStateController>()
                                                                      .safeAppState();
                                                                })),
                                                ]),
                                          ),
                                        )),
                                icon: Row(
                                  children: [
                                    const Icon(
                                      Icons.category_rounded,
                                      size: 14.0,
                                    ),
                                    const SizedBox(width: 5.0),
                                    Text('category'.tr)
                                  ],
                                )),
                            const SizedBox(width: 10.0),
                            IconButton.filledTonal(
                                visualDensity: VisualDensity.compact,
                                onPressed: () => {
                                      showDateRangePicker(
                                          context: context,
                                          firstDate: filteredTransList
                                                  .isNotEmpty
                                              ? filteredTransList.last.createdAt
                                              : DateTime.now(),
                                          lastDate:
                                              filteredTransList.isNotEmpty
                                                  ? filteredTransList
                                                      .first.createdAt
                                                  : DateTime.now(),
                                          helpText: 'Choose timespan'.tr,
                                          saveText: 'done'.tr),
                                    },
                                icon: Row(
                                  children: [
                                    const Icon(
                                      Icons.date_range_rounded,
                                      size: 14.0,
                                    ),
                                    const SizedBox(width: 5.0),
                                    Text('Timespan'.tr)
                                  ],
                                )),
                          ],
                        ),
                        const SizedBox(height: 10.0),
                        Text(
                          'transactions'.tr,
                          style: const TextStyle(
                              fontSize: 14.0, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 5.0),
                        ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: filteredTransList.length,
                          itemBuilder: (context, index) => TransactionListtile(
                              transaction: filteredTransList[index]),
                        ),
                        const SizedBox(height: 65.0)
                      ]);
                });
          }),
    );
  }

  List<LineChartBarData> buildBarData(List<Transaction> transactionsList) {
    List<LineChartBarData> categoryBars = [];
    List<Category> selectedCategories = [];
    List<Transaction> barDataList = transactionsList.toList();
    barDataList.sort((a, b) => a.createdAt.compareTo(b.createdAt));
    for (String categoryId in appState.detailsTransactionsFilter) {
      if (locator
          .get<CategoriesController>()
          .categories
          .containsKey(categoryId)) {
        selectedCategories
            .add(locator.get<CategoriesController>().categories[categoryId]!);
      }
    }
    for (Category category
        in locator.get<CategoriesController>().categories.values) {
      List<FlSpot> categoryDataPoints = [const FlSpot(0.0, 0.0)];
      int xVal = 1;
      for (Transaction transaction in barDataList) {
        if (transaction.categoryId == category.id) {
          categoryDataPoints
              .add(FlSpot(xVal.toDouble(), transaction.amount.abs()));
          xVal++;
        }
      }
      categoryBars.add(LineChartBarData(
          spots: categoryDataPoints,
          isCurved: false,
          color: Color(category.colorValue),
          barWidth: 2,
          isStrokeCapRound: true,
          dotData: FlDotData(
              show: true,
              getDotPainter: (spot, percent, barData, index) =>
                  FlDotCirclePainter(
                      radius: 2.5,
                      color: Color(category.colorValue),
                      strokeColor: Colors.transparent))));
    }
    return categoryBars;
  }
}
