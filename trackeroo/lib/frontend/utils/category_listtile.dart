import 'package:flutter/material.dart';
import 'package:trackeroo/frontend/views/edit_category_view.dart';
import 'package:trackeroo/logic/constants/selecable_colors.dart';
import 'package:trackeroo/logic/models/category.dart';
import 'package:trackeroo/logic/models/transaction.dart';
import 'package:trackeroo/logic/services/locator.dart';
import 'package:trackeroo/logic/services/transactions_controller.dart';

class CategoryListtile extends StatelessWidget {
  const CategoryListtile({Key? key, required this.category}) : super(key: key);

  final Category category;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 11.0),
      margin: const EdgeInsets.only(bottom: 5.0),
      decoration: BoxDecoration(
        border: Border.all(),
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                width: 50.0,
                height: 50.0,
                decoration: BoxDecoration(
                  color: Color(category.colorValue),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Icon(
                  IconData(
                    category.iconCodePoint,
                    fontFamily: category.iconFontFamily,
                  ),
                  color: Colors.white,
                  size: 30.0,
                ),
              ),
              const SizedBox(width: 15.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    category.title,
                    style: const TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  !category.budget.isNegative ?  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Budget: ${category.budget.toStringAsFixed(2)} €'
                      ),
                      const SizedBox(height: 3.0),
                      Stack(
                        children: [
                          Container(
                            height: 3.0,
                            width: 240,
                            color: Colors.grey[300],
                          ),
                          buildOverlayContainer(context)
                        ],
                      )
                    ],
                  ) : const SizedBox()
                ],
              ),
            ],
          ),
          IconButton(
            padding: const EdgeInsets.all(0),
            icon: const Icon(Icons.edit, size: 14.0),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => EditCategoryView(category: category),
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget buildOverlayContainer(BuildContext context) {
    DateTime dateTimeNow = DateTime.now();
    double maxWidth = 240;
    double relativeWidth = 0;
    double sum = 0;

    for(Transaction transaction in locator.get<TransactionsController>().transactionsList) {
      if (transaction.categoryId == category.id && transaction.createdAt.month == dateTimeNow.month && transaction.createdAt.year == dateTimeNow.year) {
        sum += transaction.amount;
      }
      if(sum.abs() >= category.budget) {
        return Container(
          height: 3.0,
          width: maxWidth,
          color: Theme.of(context).colorScheme.error,
        );
      }
    }
    relativeWidth = maxWidth * (sum.abs() / category.budget);
    return Container(
      height: 3.0,
      width: relativeWidth,
      color: selectableColors['Betty']!.color,
    );
  }
}
