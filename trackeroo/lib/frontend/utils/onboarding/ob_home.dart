import 'package:flutter/material.dart';

class OnboardingHome extends StatelessWidget {
  const OnboardingHome({super.key});

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
      child: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Icon(Icons.arrow_forward_ios_rounded, size: 30.0),
                SizedBox(width: 20.0),
                Text(
                  'Home',
                  style: TextStyle(
                    fontSize: 40.0,
                    fontWeight: FontWeight.w300
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Text(
              'Get an overview of your balance and average spending, aswell as your latest transactions.',
              style: TextStyle(
                fontSize: 16.0
              ),
            ),
            Expanded(
              child: Center(
                child: Icon(
                  Icons.home_rounded,
                  size: 200.0,
                )
              )
            ),
            SizedBox(height: 100.0)
          ]
        ),
      ),
    );
  }
}
