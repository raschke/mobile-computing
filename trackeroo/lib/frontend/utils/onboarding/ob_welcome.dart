import 'package:flutter/material.dart';

class OnboardingWelcome extends StatelessWidget {
  const OnboardingWelcome({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Welcome',
              style: TextStyle(
                fontSize: 40.0,
                fontWeight: FontWeight.w300
              ),
            ),
            const Text(
              'to Trackeroo',
              style: TextStyle(
                fontSize: 16.0
              ),
            ),
            const SizedBox(height: 30.0),
            const Text('This is a University Project.'),
            const SizedBox(height: 5.0),
            const Text('It collects no data and does not require Internet connection.'),
            Expanded(
              child: Center(
                child: Image.asset(
                  'assets/icon/kangaroo-circle.png',
                  width: 250.0,
                )
              )
            ),
            const Text('Created by Florian Schindler, Hannes Raschke, Michelle Kehl und Celine Schmitt'),
            const SizedBox(height: 90.0)
          ]
        ),
      ),
    );
  }
}
