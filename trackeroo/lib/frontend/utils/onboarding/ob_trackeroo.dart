// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

class OnboardingTrackeroo extends StatefulWidget {
  const OnboardingTrackeroo({super.key});

  @override
  State<OnboardingTrackeroo> createState() => _OnboardingTrackerooState();
}

class _OnboardingTrackerooState extends State<OnboardingTrackeroo> {
  SMITrigger? _trigger;

  void _onRiveInit(Artboard artboard) {
    final controller = StateMachineController.fromArtboard(artboard, 'State Machine 1')!;
    artboard.addController(controller);
    _trigger = controller.findInput<bool>('Trigger 1') as SMITrigger;
    Future.delayed(const Duration(milliseconds: 1000), () {
      _hitTrigger();
    });
  }

  void _hitTrigger() => _trigger?.fire();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Trackeroo',
              style: TextStyle(
                fontSize: 40.0,
                fontWeight: FontWeight.w300
              ),
            ),
            const Text(
              'enables you to tackle your finances and keep an eye on your spending.',
              style: TextStyle(
                fontSize: 16.0
              ),
            ),
            GestureDetector(
              onTap: _hitTrigger,
              child: Center(
                child: SizedBox(
                  width: 300.0,
                  height: 500.0,
                  child: RiveAnimation.asset(
                    'assets/rive/piggy.riv',
                    onInit: _onRiveInit,
                    animations: const ['Coinflip'],
                    fit: BoxFit.contain
                  ),
                ),
              ),
            ),
            const Center(
              child: Text(
                '(you are allowed to pet the pig)',
                style: TextStyle(
                  fontSize: 10.0
                ),
              ),
            )
          ]
        ),
      ),
    );
  }
}
