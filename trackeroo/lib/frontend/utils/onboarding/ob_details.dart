import 'package:flutter/material.dart';

class OnboardingDetails extends StatelessWidget {
  const OnboardingDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
      child: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Icon(Icons.arrow_forward_ios_rounded, size: 30.0),
                SizedBox(width: 20.0),
                Text(
                  'Details',
                  style: TextStyle(
                    fontSize: 40.0,
                    fontWeight: FontWeight.w300
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Text(
              'Deep dive into all of your transactions and analyze your spending. ',
              style: TextStyle(
                fontSize: 16.0
              ),
            ),
            Expanded(
              child: Center(
                child: Icon(
                  Icons.bar_chart,
                  size: 200.0,
                )
              )
            ),
            SizedBox(height: 100.0)
          ]
        ),
      ),
    );
  }
}
