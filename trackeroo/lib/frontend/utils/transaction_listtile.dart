import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trackeroo/frontend/views/edit_transaction_view.dart';
import 'package:trackeroo/logic/models/category.dart';
import 'package:trackeroo/logic/models/transaction.dart';
import 'package:trackeroo/logic/services/categories_controller.dart';
import 'package:trackeroo/logic/services/locator.dart';
import 'package:trackeroo/logic/services/transactions_controller.dart';

class TransactionListtile extends StatefulWidget {
  const TransactionListtile({super.key, required this.transaction});

  final Transaction transaction;

  @override
  State<TransactionListtile> createState() => _TransactionListtileState();
}

class _TransactionListtileState extends State<TransactionListtile> {
  final monthsGer = const [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  @override
  Widget build(BuildContext context) {
    Category category = locator
        .get<CategoriesController>()
        .catBox
        .get(widget.transaction.categoryId)!;
    return Padding(
      padding: const EdgeInsets.only(bottom: 5.0),
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(12.0)),
        child: Dismissible(
          key: UniqueKey(),
          direction: DismissDirection.startToEnd,
          onDismissed: (_) => deleteTransaction(context),
          background: Container(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 12.0),
            color: Theme.of(context).colorScheme.errorContainer,
            alignment: Alignment.centerLeft,
            child: Icon(
              Icons.delete,
              color: Theme.of(context).colorScheme.error,
            ),
          ),
          child: GestureDetector(
            onTap: () => showModalBottomSheet<void>(
              showDragHandle: true,
              context: context,
              builder: (BuildContext context) {
                return buildModalContent(widget.transaction, category, context);
              },
            ),
            child: Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 11.0),
              color: Theme.of(context).colorScheme.secondaryContainer,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        width: 50.0,
                        height: 50.0,
                        decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8.0)),
                            color: Theme.of(context).colorScheme.surface),
                        child: Icon(IconData(category.iconCodePoint,
                            fontFamily: category.iconFontFamily)),
                      ),
                      const SizedBox(width: 17.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 200.0,
                            child: Text(
                              widget.transaction.title,
                              style: const TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Text(
                              "${widget.transaction.createdAt.day}.${monthsGer[widget.transaction.createdAt.month - 1]} ${widget.transaction.createdAt.year} · ${widget.transaction.createdAt.hour < 10 ? 0 : ''}${widget.transaction.createdAt.hour}:${widget.transaction.createdAt.minute < 10 ? 0 : ''}${widget.transaction.createdAt.minute}")
                        ],
                      ),
                    ],
                  ),
                  Text(
                    '${widget.transaction.amount.toStringAsFixed(2)} €',
                    style: TextStyle(
                        color: widget.transaction.amount.isNegative
                            ? Colors.red[700]
                            : Colors.green[700]),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildModalContent(
      Transaction transaction, Category category, BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                transaction.title,
                style: const TextStyle(
                    fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '${transaction.createdAt.day}.${transaction.createdAt.month}.${transaction.createdAt.year} ',
                    style: const TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    '${transaction.createdAt.hour < 10 ? 0 : ''}${transaction.createdAt.hour}:${transaction.createdAt.minute < 10 ? 0 : ''}${transaction.createdAt.minute}',
                    style: const TextStyle(fontSize: 15.0),
                  )
                ],
              ),
              const SizedBox(height: 20.0),
              Text(
                '${transaction.amount.toStringAsFixed(2)} €',
                style: const TextStyle(
                    fontSize: 40.0, fontWeight: FontWeight.w300),
              ),
              const SizedBox(height: 20.0),
              category.id != 'no_category'
                  ? Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15.0, vertical: 12.0),
                      decoration: BoxDecoration(
                          color: Color(category.colorValue),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8.0))),
                      child: Row(
                        children: [
                          Icon(IconData(category.iconCodePoint,
                              fontFamily: category.iconFontFamily)),
                          const SizedBox(width: 20.0),
                          Text(category.title)
                        ],
                      ),
                    )
                  : const SizedBox(),
              const SizedBox(height: 20.0),
              const Expanded(
                  child:
                      SizedBox()), // to be removed when more content is added
              Row(
                children: [
                  Expanded(
                    child: FilledButton.tonal(
                        onPressed: () {
                          deleteTransaction(context);
                          Navigator.of(context).pop();
                        },
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.delete_rounded,
                              size: 16.0,
                            ),
                            SizedBox(width: 10.0),
                            Text('Delete'),
                          ],
                        )),
                  ),
                  const SizedBox(width: 10.0),
                  Expanded(
                    child: FilledButton.tonal(
                        onPressed: () => {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => EditTransactionView(
                                          transaction: transaction)))
                            },
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.edit,
                              size: 16.0,
                            ),
                            SizedBox(width: 10.0),
                            Text('Edit'),
                          ],
                        )),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void deleteTransaction(BuildContext context) {
    setState(() {
      locator
          .get<TransactionsController>()
          .deleteTransaction(widget.transaction);
    });
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        'Transaction deleted'.tr,
        style: TextStyle(color: Theme.of(context).colorScheme.onSecondary),
      ),
      backgroundColor: Theme.of(context).colorScheme.secondary,
      behavior: SnackBarBehavior.floating,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0))),
      action: SnackBarAction(
          label: 'Undo'.tr,
          textColor: Theme.of(context).colorScheme.onSecondary,
          onPressed: () =>
              locator.get<TransactionsController>().undoLastDeletion()),
    ));
  }
}
