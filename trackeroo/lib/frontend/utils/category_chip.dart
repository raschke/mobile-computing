import 'package:flutter/material.dart';
import 'package:trackeroo/logic/models/category.dart';

class CategoryChip extends StatelessWidget {
  const CategoryChip({Key? key, required this.category}) : super(key: key);

  final Category category;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 5.0, bottom: 5.0),
      padding: const EdgeInsets.symmetric(horizontal: 7.0, vertical: 3.0),
      decoration: BoxDecoration(
        border: Border.all(width: 0.3),
        borderRadius: const BorderRadius.all(Radius.circular(5.0)),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
              IconData(category.iconCodePoint,
                  fontFamily: category.iconFontFamily),
              size: 16.0,
              color: Color(category.colorValue)),
          const SizedBox(width: 5.0),
          Text(category.title)
        ],
      ),
    );
  }
}
