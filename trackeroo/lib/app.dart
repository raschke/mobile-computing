import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
// ignore: unused_import
import 'package:trackeroo/app_scaffold.dart';
import 'package:trackeroo/frontend/views/onboarding_view.dart';
import 'package:trackeroo/logic/services/app_state_controller.dart';
import 'package:trackeroo/logic/services/locator.dart';
import 'package:get/get.dart';
import 'package:trackeroo/logic/models/local_string.dart';

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final AppStateController appStateController =
      locator.get<AppStateController>();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.transparent,
        systemNavigationBarDividerColor: Colors.transparent,
        systemNavigationBarIconBrightness:
            Theme.of(context).brightness == Brightness.dark
                ? Brightness.light
                : Brightness.dark,
        systemNavigationBarContrastEnforced: true,
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Theme.of(context).brightness == Brightness.dark
            ? Brightness.light
            : Brightness.dark,
      ),
      child: Builder(builder: (context) {
        final lightColorScheme =
            ColorScheme.fromSeed(seedColor: const Color(0xFF0077B6));
        final darkColorScheme = ColorScheme.fromSeed(
          seedColor: const Color(0xFF0077B6),
          brightness: Brightness.dark,
        );
        return GetMaterialApp(
          translations: LocalString(),
          locale: const Locale('en', 'US'),
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            colorScheme: lightColorScheme,
            brightness: Brightness.light,
            useMaterial3: true,
            textTheme: GoogleFonts.rubikTextTheme(ThemeData.light().textTheme)
          ),
          darkTheme: ThemeData(
            colorScheme: darkColorScheme,
            brightness: Brightness.dark,
            useMaterial3: true,
            textTheme: GoogleFonts.rubikTextTheme(ThemeData.dark().textTheme)
          ),
          // TODO: for prod comment in line 60-62, line 59 is only for testing
          home: const OnboardingView(),
          // home: appStateController.appState.isFirstOpening
          //     ? const OnboardingView()
          //     : const AppScaffold(),
        );
      }),
    );
  }
}
