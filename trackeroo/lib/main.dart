import 'package:flutter/material.dart';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:trackeroo/logic/models/category.dart';
import 'package:trackeroo/logic/models/transaction.dart';
import 'package:trackeroo/logic/services/locator.dart';
import 'package:trackeroo/logic/models/profile.dart';
import 'package:trackeroo/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  Hive.registerAdapter(TransactionAdapter());
  Hive.registerAdapter(CategoryAdapter());
  Hive.registerAdapter(ProfileAdapter());
  await setupLocatorService();
  runApp(MyApp());
}
