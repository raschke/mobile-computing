import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
part 'transaction.g.dart';

@HiveType(typeId: 1)
class Transaction {
  @HiveField(0)
  int id;

  @HiveField(1)
  String title;

  @HiveField(2)
  double amount;

  @HiveField(3)
  String categoryId;

  @HiveField(4)
  DateTime createdAt;

  @HiveField(5)
  DateTime dueDate;

  Transaction({
    this.id = -1,
    required this.title,
    required this.amount,
    required this.categoryId,
    required this.createdAt,
    required this.dueDate,
  });

  factory Transaction.fromJson(Map<String, dynamic> parsedJson) {
    return Transaction(
      id: UniqueKey().hashCode,
      title: parsedJson['title'],
      amount: parsedJson['amount'],
      categoryId: parsedJson['category_id'],
      createdAt: DateTime.parse(parsedJson['created_at']),
      dueDate: DateTime.parse(parsedJson['created_at'])
    );
  }
}
