import 'package:hive/hive.dart';

part 'category.g.dart';

@HiveType(typeId: 0)
class Category {
  @HiveField(0)
  String id;

  @HiveField(1)
  String title;

  @HiveField(2)
  int iconCodePoint;

  @HiveField(3)
  String iconFontFamily;

  @HiveField(4)
  int colorValue;

  @HiveField(5)
  double spendings;

  @HiveField(6)
  double budget;

  Category({
    required this.id,
    required this.title,
    required this.iconCodePoint,
    this.iconFontFamily = 'MaterialIcons',
    required this.colorValue,
    this.spendings = 0,
    this.budget = -1, 
  });

  factory Category.fromJson(Map<String, dynamic> parsedJson) {
    return Category(
      id: parsedJson['id'],
      title: parsedJson['title'],
      iconCodePoint: parsedJson['icon_code_point'],
      iconFontFamily: parsedJson['icon_font_family'],
      colorValue: parsedJson['color_value'],
      budget: parsedJson['budget'] ?? -1,
    );
  }
}