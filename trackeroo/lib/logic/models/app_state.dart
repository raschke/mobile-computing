class AppState {
  AppState({required this.isFirstOpening, required this.detailsTransactionsFilter});

  bool isFirstOpening;
  List<String> detailsTransactionsFilter;
}
