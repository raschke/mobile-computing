import 'package:hive/hive.dart';

part 'profile.g.dart';

@HiveType(typeId: 1)
class Profile {
  @HiveField(0)
  String name;

  @HiveField(1)
  int number;

  @HiveField(2)
  String email;

  @HiveField(3)
  String imagePath;

  Profile({
    required this.name,
    required this.number,
    required this.email,
    required this.imagePath,
  });
}
