import 'package:flutter/material.dart';

class SelectableColor {
  SelectableColor({required this.title, required this.color});

  String title;
  Color color;
}
