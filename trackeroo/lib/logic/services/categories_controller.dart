import 'package:hive/hive.dart';
import 'package:trackeroo/logic/models/category.dart';

class CategoriesController {
  CategoriesController({required this.catBox, required this.categories});

  Box<Category> catBox;
  Map<dynamic, Category> categories;

  Future<bool> saveCategory(Category category) async {
    // String id = category.title.replaceAll(' ', '_').toLowerCase();
    try {
      await catBox.put(category.id, category);
      categories[category.id] = category;
    } catch (e) {
      return false;
    }
    return true;
  }

  Future<bool> updateCategory(Category category) async {
    try {
      await catBox.put(category.id, category);
      categories[category.id] = category;
    } catch (e) {
      return false;
    }
    return true;
  }

  Future<bool> deleteCategory(String id) async {
    try {
      await catBox.delete(id);
      categories.remove(id);
    } catch (e) {
      return false;
    }
    return true;
  }
}
