import 'package:hive/hive.dart';
import 'package:trackeroo/logic/models/app_state.dart';

class AppStateController {
  AppStateController({required this.appStateBox, required this.appState});

  Box<dynamic> appStateBox;
  AppState appState;

  void safeAppState() {
    appStateBox.put('is_first_opening', appState.isFirstOpening);
    appStateBox.put('details_transactions_filter', appState.detailsTransactionsFilter);
  }
}
