import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:get_it/get_it.dart';
import 'package:trackeroo/logic/constants/selecable_colors.dart';
import 'package:trackeroo/logic/constants/selectable_icons.dart';

import 'package:trackeroo/logic/models/category.dart';
import 'package:trackeroo/logic/models/app_state.dart';
import 'package:trackeroo/logic/models/transaction.dart';
import 'package:trackeroo/logic/services/app_state_controller.dart';
import 'package:trackeroo/logic/services/categories_controller.dart';
import 'package:trackeroo/logic/services/transactions_controller.dart';
import 'package:trackeroo/logic/services/profile_controller.dart';

import '../models/profile.dart';

final locator = GetIt.instance;

Future<void> setupLocatorService() async {
  // open AppState box and load all entries from the box into the object and register the object in get_it
  var appStateBox = await Hive.openBox('app_state_box');
  Map<dynamic, dynamic> appStateMap = appStateBox.toMap();
  AppState appState = AppState(
      isFirstOpening: appStateMap['is_first_opening'] ?? true,
      detailsTransactionsFilter:
          appStateMap['details_transactions_filter'] ?? []);
  AppStateController appStateController =
      AppStateController(appStateBox: appStateBox, appState: appState);
  locator.registerLazySingleton<AppStateController>(() => appStateController);

  // load categories from box and safe to list, make list available in get_it
  Box<Category> categoriesBox = await Hive.openBox<Category>('categories_box');
  if (appState.isFirstOpening) {
    // create default categories
    categoriesBox.putAll({
      'food_and_groceries': Category(
          id: 'food_and_groceries',
          title: 'Food & Groceries',
          iconCodePoint: selectableIcons[13].codePoint,
          colorValue: selectableColors['Sorta Sage']!.color.value),
      'transport_and_car': Category(
          id: 'transport_and_car',
          title: 'Transport & Car',
          iconCodePoint: selectableIcons[27].codePoint,
          colorValue: selectableColors['Pale Yellow']!.color.value),
      'healthcare_and_drug_stores': Category(
          id: 'healthcare_and_drug_stores',
          title: 'Healthcare & Drug Stores',
          iconCodePoint: selectableIcons[14].codePoint,
          colorValue: selectableColors['Terra Cotta']!.color.value),
      'shopping': Category(
          id: 'shopping',
          title: 'Shopping',
          iconCodePoint: selectableIcons[26].codePoint,
          colorValue: selectableColors['Ocean']!.color.value),
      'bars_and_restaurants': Category(
          id: 'bars_and_restaurants',
          title: 'Bars & Restaurants',
          iconCodePoint: selectableIcons[2].codePoint,
          colorValue: selectableColors['Coral']!.color.value),
      'family_and_friends': Category(
          id: 'family_and_friends',
          title: 'Family & Friends',
          iconCodePoint: selectableIcons[11].codePoint,
          colorValue: selectableColors['Caribean']!.color.value),
      'leisure_and_entertainment': Category(
          id: 'leisure_and_entertainment',
          title: 'Leisure & Entertainment',
          iconCodePoint: selectableIcons[18].codePoint,
          colorValue: selectableColors['Mint']!.color.value),
      'media_and_electronics': Category(
          id: 'media_and_electronics',
          title: 'Media & Electronics',
          iconCodePoint: selectableIcons[21].codePoint,
          colorValue: selectableColors['Dark Olive']!.color.value),
      'education': Category(
          id: 'education',
          title: 'Education',
          iconCodePoint: selectableIcons[6].codePoint,
          colorValue: selectableColors['Eucalyptus']!.color.value),
      'household_and_utilities': Category(
          id: 'household_and_utilities',
          title: 'Household & Utilities',
          iconCodePoint: selectableIcons[16].codePoint,
          colorValue: selectableColors['Salmon']!.color.value),
      'travel_and_holidays': Category(
          id: 'travel_and_holidays',
          title: 'Travel & Holidays',
          iconCodePoint: selectableIcons[34].codePoint,
          colorValue: selectableColors['Sorta Sage']!.color.value),
      'atm': Category(
          id: 'atm',
          title: 'ATM',
          iconCodePoint: selectableIcons[1].codePoint,
          colorValue: selectableColors['Warm Stone']!.color.value)
    });
  }
  Map<dynamic, Category> categoriesMap = categoriesBox.toMap();
  CategoriesController categoriesController =
      CategoriesController(catBox: categoriesBox, categories: categoriesMap);
  locator
      .registerLazySingleton<CategoriesController>(() => categoriesController);

  Box<Transaction> transactionsBox =
      await Hive.openBox<Transaction>('transactions_box');

  // TODO: remove for prod, only here for dev/test purposes
  if (appState.isFirstOpening) {
    List<Transaction> transactionsFromJson = [];
    final String response =
        await rootBundle.loadString('assets/data/transaction_data.json');
    final jsonList = await json.decode(response);
    for (var json in jsonList) {
      transactionsFromJson.add(Transaction.fromJson(json));
    }
    transactionsBox.addAll(transactionsFromJson);
  }

  double balance = 0;
  double income = 0;
  double expenses = 0;
  for (Transaction tr in transactionsBox.values.toList()) {
    balance += tr.amount;
    if (tr.amount.isNegative) {
      expenses += tr.amount;
    } else {
      income += tr.amount;
    }
  }
  TransactionsController transactionsController = TransactionsController(
      transactionsBox: transactionsBox,
      transactionsList: transactionsBox.values.toList(),
      balance: balance,
      income: income,
      expenses: expenses);

  Box<Profile> profileBox = await Hive.openBox('profileBox');
  Profile empty = Profile(
      name: 'Max Musterman',
      number: 0123456789,
      email: 'MaxMusterman@gmail.com',
      imagePath: '');
  if (appState.isFirstOpening) {
    profileBox.put('profile', empty);
  }

  ProfileController profileController = ProfileController(
      profilBox: profileBox, profile: profileBox.get('profile') ?? empty);

  locator.registerLazySingleton<ProfileController>(() => profileController);

  // TODO: remove for prod, only here for dev/test purposes

  locator.registerLazySingleton<TransactionsController>(
      () => transactionsController);
}
