import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:trackeroo/logic/models/transaction.dart';
import 'package:trackeroo/logic/services/categories_controller.dart';
import 'package:trackeroo/logic/services/locator.dart';

class TransactionsController extends ChangeNotifier {
  TransactionsController({required this.transactionsBox, required this.transactionsList, required this.balance, required this.income, required this.expenses});

  Box<Transaction> transactionsBox;
  List<Transaction> transactionsList;
  double balance;
  double income;
  double expenses;
  late Transaction lastDeletedTransaction;

  Future<bool> saveTransaction(Transaction transaction) async {
    try {
      locator.get<CategoriesController>().categories[transaction.categoryId]?.spendings += transaction.amount;
      int id = UniqueKey().hashCode;
      transaction.id = id;
      balance += transaction.amount;
      if(transaction.amount.isNegative) {
        expenses += transaction.amount;
      } else {
        income += transaction.amount;
      }
      await transactionsBox.put(transaction.id, transaction);
      transactionsList = transactionsBox.values.toList();
    } catch (e) {
      return false;
    }
    return true;
  }

  Future<bool> updateTransaction(double oldAmount, Transaction transaction) async {
    try {
      await transactionsBox.put(transaction.id, transaction);
      transactionsList = transactionsBox.values.toList();
    } catch (e) {
      return false;
    }
    return true;
  }

  Future<bool> deleteTransaction(Transaction transaction) async {
    try {
      lastDeletedTransaction = transaction;
      balance -= transaction.amount;
      if(transaction.amount.isNegative) {
        expenses -= transaction.amount;
      } else {
        income -= transaction.amount;
      }
      await transactionsBox.delete(transaction.id);
      transactionsList = transactionsBox.values.toList();
    } catch (e) {
      return false;
    }
    return true;
  }

  Future<bool> undoLastDeletion() async {
    try {
      balance += lastDeletedTransaction.amount;

      if(lastDeletedTransaction.amount.isNegative) {
        expenses += lastDeletedTransaction.amount;
      } else {
        income += lastDeletedTransaction.amount;
      }
      await transactionsBox.put(lastDeletedTransaction.id, lastDeletedTransaction);
      transactionsList = transactionsBox.values.toList();
    } catch (e) {
      return false;
    }
    return true;
  }
}
