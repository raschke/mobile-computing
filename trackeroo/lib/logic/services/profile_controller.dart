import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:trackeroo/logic/models/profile.dart';

class ProfileController extends ChangeNotifier {
  ProfileController({required this.profilBox, required this.profile});

  Box profilBox;
  Profile profile;

  Future<void> saveProfile(Profile profile) async {
    try {
      this.profile = profile;
      profilBox.put('profile', profile);
    } catch (e) {
      return;
    }
    return;
  }
}
