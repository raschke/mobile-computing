import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trackeroo/frontend/views/edit_category_view.dart';
import 'package:trackeroo/frontend/views/edit_transaction_view.dart';
import 'package:trackeroo/frontend/views/home_view.dart';
import 'package:trackeroo/frontend/views/details_view.dart';
import 'package:trackeroo/frontend/views/categories_view.dart';
import 'package:trackeroo/logic/services/locator.dart';
import 'package:trackeroo/logic/services/transactions_controller.dart';
import 'package:trackeroo/frontend/views/settings_views.dart';

class AppScaffold extends StatefulWidget {
  const AppScaffold({Key? key}) : super(key: key);

  @override
  State<AppScaffold> createState() => _AppScaffoldState();
}

class _AppScaffoldState extends State<AppScaffold> {
  int index = 0;

  final screens = [
    const HomeView(),
    const DetailsView(),
    const CategoriesView(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Trackeroo',
            style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold)),
        elevation: 0.0,
        scrolledUnderElevation: 0.0,
        actions: [
          IconButton.filledTonal(
              onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SettingsView()))
                  },
              padding: const EdgeInsets.all(12.0),
              icon: const Icon(Icons.person)),
          const SizedBox(width: 16.0)
        ],
      ),
      body: screens[index],
      bottomNavigationBar: NavigationBarTheme(
        data: NavigationBarThemeData(
          backgroundColor:
              Theme.of(context).colorScheme.secondaryContainer.withOpacity(0.3),
          indicatorColor: Theme.of(context).colorScheme.secondaryContainer,
          labelTextStyle: MaterialStateProperty.all(const TextStyle(
            fontSize: 15.0,
            fontWeight: FontWeight.w500,
          )),
        ),
        child: NavigationBar(
          height: 70,
          selectedIndex: index,
          onDestinationSelected: (index) => setState(() => this.index = index),
          destinations: [
            const NavigationDestination(
                icon: Icon(Icons.home_outlined),
                selectedIcon: Icon(Icons.home_rounded),
                label: 'Home'),
            const NavigationDestination(
                icon: Icon(Icons.bar_chart),
                selectedIcon: Icon(Icons.bar_chart),
                label: 'Details'),
            NavigationDestination(
                icon: const Icon(Icons.category_outlined),
                selectedIcon: const Icon(Icons.category_rounded),
                label: 'Categories'.tr)
          ],
        ),
      ),
      floatingActionButton: locator
              .get<TransactionsController>()
              .transactionsList
              .isNotEmpty
          ? FloatingActionButton(
              onPressed: () => fabOnPressed(),
              child: const Icon(Icons.add_rounded),
            )
          : FloatingActionButton.extended(
              onPressed: () => fabOnPressed(),
              label: Row(
                children: [
                  const Icon(Icons.add_rounded),
                  const SizedBox(width: 10.0),
                  Text(index == 2 ? 'add Category'.tr : 'add Transaction'.tr)
                ],
              )),
    );
  }

  void fabOnPressed() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => index == 2
                ? const EditCategoryView()
                : const EditTransactionView()));
  }
}
